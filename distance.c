#include <stdio.h>
#include <math.h>
int main()
{
    float x1,y1,x2,y2,d;
    printf("Enter the coordinates of first point(x1,y1)\n");
    scanf("%f%f",&x1,&y1);
    printf("Enter the coordinates of second point(x2,y2)\n");
    scanf("%f%f",&x2,&y2);
    d=(float)sqrt(pow((x2-x1),2) + pow((y2-y1),2));
    printf("The distance between two points is %f",d);
  return 0;
}

