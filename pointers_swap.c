#include <stdio.h>
void swap(int *p,int *q); 
int main()
{
    int a,b;
    printf("Enter the value of a\n");
    scanf("%d",&a);
    printf("Enter the value of b\n");
    scanf("%d",&b);
    printf("Before swapping\n a=%d\n b=%d\n",a,b);
    swap(&a,&b);
    printf("After swapping\n a=%d\n b=%d\n",a,b);
    return 0;
}
void swap(int *p,int *q)
{
    int t;
    t=*p;
    *p=*q;
    *q=t;   
}